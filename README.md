# Starting the twitter-client app

1. Ensure Java 11 and Gradle are installed on your machine.
1. In the terminal, navigate to the twitter-client directory
1. Run `./gradlew clean build bootRun`
1. The application should now be running at http://localhost:8080.

## Running tests
In the terminal, run `./gradlew clean test`

# The API
Twitter-client provides a REST API with several endpoints:

## GET /user
Returns a list of all users, in this format:
```json
[
    {
        "id": "random UUID",
        "name": "username",
        "tweetIds": ["ID of most recent tweet", "...", "ID of oldest tweet"]
    }
]
```

## GET /user/:id
`:id` refers to the UUID assigned to a user.
Returns the user with that ID, in this format:
```json
{
    "id": "random UUID",
    "name": "username",
    "tweetIds": ["ID of most recent tweet", "...", "ID of oldest tweet"]
}
```

## GET /user/:id/tweet
Returns a list of all tweets by the user with that ID, in this format:
```json5
[
  {
    "id": "random UUID",
    "userId": "ID of the user who made the tweet",
    "tweet": "witty message by the user",
    "createdAt": 1606333283 //epoch milliseconds
  }
]
```
The most recent tweet will be first in the list, followed by the next
most recent tweet, etc.

## POST /user
Create a new user.
The request should be made in this format:
```json
{
  "name": "username"
}
```

If the request is successful, the server will return 201 with the
user in the response body:
```json
{
    "id": "random UUID",
    "name": "requestedusername",
    "tweetIds": []
}
```

## GET /tweet
Returns a list of all tweets, in this format:
```json5
[
  {
    "id": "random UUID",
    "userId": "ID of the user who made the tweet",
    "tweet": "witty message by the user",
    "createdAt": 1606333283 //epoch milliseconds
  }
]
```
These tweets are returned in no particular order.

## GET /tweet/:id
Returns the tweet with that ID, in this format:
```json5
{
  "id": "random UUID",
  "userId": "ID of the user who made the tweet",
  "tweet": "witty message by the user",
  "createdAt": 1606333283 //epoch milliseconds
}
```

## POST /tweet
Create a new tweet. The request body should use this format:
```json5
{
    "user_name": "name of user who is making the tweet",
    "tweet": "that user's witty thoughts"
}
```

If successful, the server will return a 201 response with this response
body:
```json5
{
  "id": "random UUID",
  "userId": "ID of the user who made the tweet",
  "tweet": "same as tweet in request",
  "createdAt": 1606333283 //epoch milliseconds
}
```

## GET /search
Get a list of tweets meeting certain criteria. Any combination
of the following request parameters can be used:

* `start_time` (Number): get Tweets made at this time (in epoch milliseconds) or later
* `end_time` (Number): get Tweets made at this time (in epoch milliseconds) or earlier
* `key_word` (String): get Tweets that contain this substring

Tweets are returned in no particular order, in this format:
```json5
[
  {
    "id": "random UUID",
    "userId": "ID of the user who made the tweet",
    "tweet": "witty message by the user",
    "createdAt": 1606333283 //epoch milliseconds
  }
]
```