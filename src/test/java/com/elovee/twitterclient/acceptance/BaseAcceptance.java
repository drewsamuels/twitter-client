package com.elovee.twitterclient.acceptance;

import com.elovee.twitterclient.model.Tweet;
import com.elovee.twitterclient.model.TweetRequest;
import com.elovee.twitterclient.model.User;
import com.elovee.twitterclient.model.UserRequest;
import com.elovee.twitterclient.repo.UserRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public abstract class BaseAcceptance {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    UserRepo userRepo;

    ObjectMapper objectMapper = new ObjectMapper();

    String performGetToEndpoint(String endpoint, HttpStatus expectedStatus) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.get(endpoint)
            .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().is(expectedStatus.value()))
            .andReturn()
            .getResponse()
            .getContentAsString();
    }

    String performPost(String url, String content, int expectedStatus) throws Exception {
        return mockMvc.perform(post(url)
            .content(content)
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().is(expectedStatus))
            .andReturn()
            .getResponse()
            .getContentAsString();
    }

    User createUser(String userName) throws Exception {
        UserRequest request = new UserRequest(userName);
        return performPostUser(request, HttpStatus.CREATED);
    }

    User performPostUser(UserRequest request, HttpStatus expectedStatus) throws Exception {
        String requestString = objectMapper.writeValueAsString(request);
        String response = performPost("/user", requestString, expectedStatus.value());
        return objectMapper.readValue(response, User.class);
    }

    Tweet performPostTweet(TweetRequest request, HttpStatus expectedStatus) throws Exception {
        String requestString = objectMapper.writeValueAsString(request);
        String response = performPost("/tweet", requestString, expectedStatus.value());
        return objectMapper.readValue(response, Tweet.class);
    }

    Tweet createTweet(User user, String tweet) throws Exception {
        TweetRequest request = new TweetRequest(user.getName(), tweet);
        return performPostTweet(request, HttpStatus.CREATED);
    }

    void seedDatabaseWithRandomUsers() throws Exception {
        createUser("user-1");
        createUser("other-user");
        createUser("valid-user");
    }
}
