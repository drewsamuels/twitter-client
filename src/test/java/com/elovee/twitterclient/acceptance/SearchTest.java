package com.elovee.twitterclient.acceptance;

import com.elovee.twitterclient.model.Tweet;
import com.elovee.twitterclient.model.User;
import com.elovee.twitterclient.repo.TweetRepo;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SearchTest extends BaseAcceptance {

    @Autowired
    private TweetRepo tweetRepo;

    private Tweet mockTweet1;
    private Tweet mockTweet2;
    private Tweet mockTweet3;
    private Tweet mockTweet4;

    @BeforeEach
    void setup() throws Exception {
        userRepo.deleteAll();
        tweetRepo.deleteAll();

        seedDatabaseWithMockTweets();
    }

    @Test
    @DisplayName("search, when called with no parameters, returns 400 bad request")
    void search_noParams() throws Exception {
        search(HttpStatus.BAD_REQUEST, null, null, null);
    }

    @Test
    @DisplayName("search, when startTime is defined, excludes tweets from before that time")
    void search_startTime() throws Exception {
        long startTime = 30L;
        List<Tweet> actual = search(HttpStatus.OK, startTime, null, null);

        assertEquals(2, actual.size());
        assertTrue(actual.contains(mockTweet3));
        assertTrue(actual.contains(mockTweet4));
    }

    @Test
    @DisplayName("search, when endTime is defined, excludes tweets from after that time")
    void search_endTime() throws Exception {
        long endTime = 20L;
        List<Tweet> actual = search(HttpStatus.OK, null, endTime, null);

        assertEquals(2, actual.size());
        assertTrue(actual.contains(mockTweet1));
        assertTrue(actual.contains(mockTweet2));
    }

    @Test
    @DisplayName("search, when keyWord is defined, only returns tweets that contain that substring")
    void search_keyWord() throws Exception {
        String keyword = "dog";
        List<Tweet> actual = search(HttpStatus.OK, null, null, keyword);

        assertEquals(2, actual.size());
        assertTrue(actual.contains(mockTweet1));
        assertTrue(actual.contains(mockTweet3));
    }

    @Test
    @DisplayName("search, when all 3 params supplied, only returns tweets matching all 3")
    void search_allParams() throws Exception {
        List<Tweet> actual = search(HttpStatus.OK, 15L, 35L, "bear");
        assertEquals(singletonList(mockTweet2), actual);
    }

    private void seedDatabaseWithMockTweets() throws Exception {
        User user1 = createUser("name1");
        User user2 = createUser("name2");

        mockTweet1 = new Tweet(user1.getId(), "tweet1 dog cat moose", 10L);
        mockTweet2 = new Tweet(user1.getId(), "tweet2 fox moose bear", 20L);
        mockTweet3 = new Tweet(user2.getId(), "tweet3 moose fox dog", 30L);
        mockTweet4 = new Tweet(user1.getId(), "tweet4 fox mouse hen", 40L);

        tweetRepo.save(mockTweet1);
        tweetRepo.save(mockTweet2);
        tweetRepo.save(mockTweet3);
        tweetRepo.save(mockTweet4);
    }

    private List<Tweet> search(HttpStatus expectedStatus, Long startTime, Long endTime, String keyWord) throws Exception {
        String uri = buildRequestAttributes(startTime, endTime, keyWord);
        String result = performGetToEndpoint(uri, expectedStatus);
        return expectedStatus.is2xxSuccessful() ? mapListFromString(result) : null;
    }

    private String buildRequestAttributes(Long startTime, Long endTime, String keyWord) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromPath("/search");
        if (startTime != null) {
            builder = builder.queryParam("start_time", startTime);
        }
        if (endTime != null) {
            builder = builder.queryParam("end_time", endTime);
        }
        if (keyWord != null) {
            builder = builder.queryParam("key_word", keyWord);
        }
        return builder.toUriString();
    }

    private List<Tweet> mapListFromString(String raw) throws JsonProcessingException {
        return asList(objectMapper.readValue(raw, Tweet[].class));
    }
}
