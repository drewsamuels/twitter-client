package com.elovee.twitterclient.acceptance;

import com.elovee.twitterclient.model.Tweet;
import com.elovee.twitterclient.model.TweetRequest;
import com.elovee.twitterclient.model.User;
import com.elovee.twitterclient.repo.TweetRepo;
import org.apache.logging.log4j.util.Strings;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TweetTest extends BaseAcceptance {

    @Autowired
    private TweetRepo tweetRepo;

    @BeforeEach
    void setup() {
        tweetRepo.deleteAll();
        userRepo.deleteAll();
    }

    @Test
    @DisplayName("POST /tweet, when called with valid POST body for a valid user, returns 201 with Tweet in response")
    void post_tweet_valid() throws Exception {
        String username = "valid-user";
        User user = createUser(username);

        String tweet = "valid tweet";
        TweetRequest validRequest = new TweetRequest(username, tweet);
        Tweet response = performPostTweet(validRequest, HttpStatus.CREATED);

        assertEquals(user.getId(), response.getUserId());
        assertEquals(tweet, response.getTweet());
        assertTrue(Strings.isNotEmpty(response.getId()));
    }

    @Test
    @DisplayName("POST /tweet, when called with a user that does not exist, returns 404 Not Found")
    void post_tweet_noUser() throws Exception {
        seedDatabaseWithRandomUsers();
        String tweet = "valid tweet";
        TweetRequest validRequest = new TweetRequest("user-that-does-not-exist", tweet);
        performPostTweet(validRequest, HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("POST /tweet, when no tweet is defined, returns 400 Bad Request")
    void post_tweet_invalid_tweet() throws Exception {
        createUser("user");
        TweetRequest validRequest = new TweetRequest("user", null);
        performPostTweet(validRequest, HttpStatus.BAD_REQUEST);
    }

    @Test
    @DisplayName("POST /tweet, when no user is defined, returns 400 Bad Request")
    void post_tweet_invalid_user() throws Exception {
        TweetRequest validRequest = new TweetRequest(null, "my thoughts");
        performPostTweet(validRequest, HttpStatus.BAD_REQUEST);
    }

    @Test
    @DisplayName("GET /tweet returns all tweets")
    void get_tweet() throws Exception {
        User user = createUser("my-user-name");
        String tweetContents1 = "my valid thoughts";
        String tweetContents2 = "my other thoughts";
        Tweet tweet1 = createTweet(user, tweetContents1);
        Tweet tweet2 = createTweet(user, tweetContents2);

        List<Tweet> actual = performGetTweet();
        assertEquals(2, actual.size());
        assertTrue(actual.contains(tweet1));
        assertTrue(actual.contains(tweet2));
    }

    @Test
    @DisplayName("GET /tweet, when no tweets exist, returns empty list")
    void get_tweet_empty() throws Exception {
        List<Tweet> actual = performGetTweet();
        assertEquals(emptyList(), actual);
    }

    @Test
    @DisplayName("GET /tweet/:id, when tweet exists, returns tweet with 200 response")
    void get_tweet_id() throws Exception {
        String username = "current-user";
        User user = createUser(username);
        String relevantTweetContents = "my relevant thoughts";
        String irrelevantTweetContents = "my other thoughts";
        Tweet relevantTweet = createTweet(user, relevantTweetContents);
        createTweet(user, irrelevantTweetContents);

        Tweet actual = performGetTweet(relevantTweet.getId(), HttpStatus.OK);
        assertEquals(relevantTweet, actual);
    }

    @Test
    @DisplayName("GET /tweet/:id, when tweet does not exist, returns 404 Not Found")
    void get_tweet_id_notFound() throws Exception {
        seedDatabaseWithRandomTweets();
        performGetTweet("fake-id", HttpStatus.NOT_FOUND);
    }

    private List<Tweet> performGetTweet() throws Exception {
        String endpoint = "/tweet";
        String response = performGetToEndpoint(endpoint, HttpStatus.OK);
        return asList(objectMapper.readValue(response, Tweet[].class));
    }

    private Tweet performGetTweet(String id, HttpStatus expectedStatus) throws Exception {
        String endpoint = "/tweet/" + id;
        String response = performGetToEndpoint(endpoint, expectedStatus);
        return objectMapper.readValue(response, Tweet.class);
    }

    private void seedDatabaseWithRandomTweets() throws Exception {
        User user = createUser("my-user-name");
        String tweetContents1 = "my valid thoughts";
        String tweetContents2 = "my other thoughts";
        createTweet(user, tweetContents1);
        createTweet(user, tweetContents2);
    }
}
