package com.elovee.twitterclient.acceptance;

import com.elovee.twitterclient.model.Tweet;
import com.elovee.twitterclient.model.User;
import com.elovee.twitterclient.model.UserRequest;
import org.apache.logging.log4j.util.Strings;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;

public class UserTest extends BaseAcceptance {

    @BeforeEach
    void setup() {
        userRepo.deleteAll();
    }

    @Test
    @DisplayName("POST /user, when called with valid POST body, returns 201 with User in response")
    void post_user_valid() throws Exception {
        String userName = "valid-user";
        UserRequest validRequest = new UserRequest(userName);

        User response = performPostUser(validRequest, HttpStatus.CREATED);
        assertEquals(userName, response.getName());
        assertFalse(Strings.isEmpty(response.getId()));
    }

    @Test
    @DisplayName("POST /user, when called with invalid body, returns 400 Bad Request")
    void post_user_invalid() throws Exception {
        UserRequest emptyRequest = new UserRequest(null);

        performPostUser(emptyRequest, HttpStatus.BAD_REQUEST);
    }

    @Test
    @DisplayName("POST /user, when called with user name that is already taken, returns 409 conflict")
    void post_user_conflict() throws Exception {
        String userName = "user-1";
        createUser(userName);
        UserRequest duplicateRequest = new UserRequest(userName);

        performPostUser(duplicateRequest, HttpStatus.CONFLICT);
    }

    @Test
    @DisplayName("GET /user, when multiple users exist, returns all users")
    void get_user() throws Exception {
        String userName1 = "user-1";
        User user1 = createUser(userName1);
        String userName2 = "user-2";
        User user2 = createUser(userName2);

        List<User> response = performGetUser();
        assertEquals(2, response.size());
        assertTrue(response.contains(user1));
        assertTrue(response.contains(user2));
    }

    @Test
    @DisplayName("GET /user, when no users exist, returns empty list")
    void get_user_empty() throws Exception {
        List<User> response = performGetUser();
        assertEquals(emptyList(), response);
    }

    @Test
    @DisplayName("GET /user/:id, when called with ID of existing user, returns that user")
    void get_user_with_id() throws Exception {
        String relevantUserName = "user-1";
        User relevantUser = createUser(relevantUserName);
        String otherUserName = "user-2";
        createUser(otherUserName);

        User actual = performGetUserById(relevantUser.getId(), HttpStatus.OK);
        assertEquals(relevantUser, actual);
    }

    @Test
    @DisplayName("GET /user/:id, when called with ID of a nonexisting user, returns 404 Not Found")
    void get_user_404() throws Exception {
        String userName1 = "user-1";
        createUser(userName1);
        String userName2 = "user-2";
        createUser(userName2);

        performGetUserById("id-that-does-not-exist", HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("GET /user/:id/tweet, when called with ID of a nonexisting user, returns 404 Not Found")
    void get_user_tweets_404() throws Exception {
        seedDatabaseWithRandomUsers();
        performGetUserTweets("user-that-does-not-exist", HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("GET /user/:id/tweet, for user who has not tweeted, returns empty list")
    void get_user_tweets_empty() throws Exception {
        seedDatabaseWithRandomUsers();
        User relevantUser = createUser("relevant-name");

        List<Tweet> tweets = performGetUserTweets(relevantUser.getId(), HttpStatus.OK);
        assertEquals(emptyList(), tweets);
    }

    @Test
    @DisplayName("GET /user/:id/tweet, for user who has tweeted, returns tweets")
    void get_user_tweets() throws Exception {
        seedDatabaseWithRandomUsers();
        User relevantUser = createUser("relevant-name");
        Tweet tweet1 = createTweet(relevantUser, "tweet idea 1");
        Tweet tweet2 = createTweet(relevantUser, "tweet idea 2");

        List<Tweet> actual = performGetUserTweets(relevantUser.getId(), HttpStatus.OK);
        assertEquals(2, actual.size());
        assertTrue(actual.contains(tweet1));
        assertTrue(actual.contains(tweet2));
    }

    private List<User> performGetUser() throws Exception {
        String response = performGetToEndpoint("/user", HttpStatus.OK);
        return asList(objectMapper.readValue(response, User[].class));
    }

    private User performGetUserById(String id, HttpStatus expectedStatus) throws Exception {
        String response = performGetToEndpoint("/user/" + id, expectedStatus);
        return objectMapper.readValue(response, User.class);
    }

    private List<Tweet> performGetUserTweets(String userId, HttpStatus expectedStatus) throws Exception {
        String response = performGetToEndpoint("/user/" + userId + "/tweet", expectedStatus);
        return expectedStatus.is2xxSuccessful() ? asList(objectMapper.readValue(response, Tweet[].class)) : null;
    }
}
