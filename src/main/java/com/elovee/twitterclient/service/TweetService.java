package com.elovee.twitterclient.service;

import com.elovee.twitterclient.exception.NotFoundException;
import com.elovee.twitterclient.model.Tweet;
import com.elovee.twitterclient.model.TweetRequest;
import com.elovee.twitterclient.model.User;
import com.elovee.twitterclient.repo.TweetRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TweetService {

    private final TweetRepo tweetRepo;
    private final UserService userService;

    public Tweet createTweet(TweetRequest request) throws NotFoundException {
        User user = userService.getUserByUsername(request.getUser_name());
        Tweet tweet = new Tweet(request, user.getId());
        tweetRepo.save(tweet);
        user.addTweet(tweet);
        userService.updateUser(user);
        return tweet;
    }

    public List<Tweet> getAllTweets() {
        return tweetRepo.getAll();
    }

    public List<Tweet> searchTweets(Long startTime, Long endTime, String keyWord) {
        return getAllTweets().stream()
            .filter(filterByStartTime(startTime))
            .filter(filterByEndTime(endTime))
            .filter(filterByKeyWord(keyWord))
            .collect(Collectors.toList());
    }

    public List<Tweet> getTweetsForUser(String userId) throws NotFoundException {
        User user = userService.getUser(userId);
        return user.getTweetIds().stream()
            .map(this::getTweet)
            .collect(Collectors.toList());
    }

    public Tweet getTweet(String id) throws NotFoundException {
        Tweet tweet = tweetRepo.getById(id);
        if (Objects.isNull(tweet)) {
            throw new NotFoundException("No tweet exists with id \"" + id + "\"");
        }
        return tweet;
    }

    private Predicate<Tweet> filterByStartTime(Long startTime) {
        return tweet -> startTime == null || tweet.getCreatedAt() >= startTime;
    }

    private Predicate<Tweet> filterByEndTime(Long endTime) {
        return tweet -> endTime == null || tweet.getCreatedAt() <= endTime;
    }

    private Predicate<Tweet> filterByKeyWord(String keyWord) {
        return tweet -> keyWord == null || tweet.getTweet().contains(keyWord);
    }
}
