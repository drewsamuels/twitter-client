package com.elovee.twitterclient.service;

import com.elovee.twitterclient.exception.DuplicateException;
import com.elovee.twitterclient.exception.NotFoundException;
import com.elovee.twitterclient.model.User;
import com.elovee.twitterclient.model.UserRequest;
import com.elovee.twitterclient.repo.UserRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class UserService {

    private final UserRepo userRepo;

    public User getUserByUsername(String username) throws NotFoundException {
        User user = userRepo.getByName(username);
        if (Objects.isNull(user)) {
            throw new NotFoundException("No user exists with name \"" + username + "\"");
        }
        return user;
    }

    public User createUser(UserRequest request) throws DuplicateException {
        String name = request.getName();
        if (userRepo.usernameExists(name)) {
            throw new DuplicateException("user_name \"" + name + "\" is already in use");
        }
        User user = new User(request);
        userRepo.save(user);
        return user;
    }

    public void updateUser(User newValue) {
        userRepo.save(newValue);
    }

    public List<User> getAllUsers() {
        return userRepo.getAll();
    }

    public User getUser(String id) throws NotFoundException {
        User user = userRepo.getById(id);
        if (Objects.isNull(user)) {
            throw new NotFoundException("No user exists with id \"" + id + "\"");
        }
        return user;
    }
}
