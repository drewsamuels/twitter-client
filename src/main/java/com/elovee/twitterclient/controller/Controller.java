package com.elovee.twitterclient.controller;

import com.elovee.twitterclient.exception.BadRequestException;
import com.elovee.twitterclient.exception.DuplicateException;
import com.elovee.twitterclient.exception.NotFoundException;
import com.elovee.twitterclient.model.*;
import com.elovee.twitterclient.service.TweetService;
import com.elovee.twitterclient.service.UserService;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class Controller {

    private final UserService userService;
    private final TweetService tweetService;

    @PostMapping("/user")
    @ResponseStatus(HttpStatus.CREATED)
    public User createUser(@RequestBody UserRequest userRequest) throws BadRequestException, DuplicateException {
        if (!isValid(userRequest)) {
            throw new BadRequestException("Must specify name");
        }
        return userService.createUser(userRequest);
    }

    @GetMapping("/user")
    @ResponseStatus(HttpStatus.OK)
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/user/{id}")
    @ResponseStatus(HttpStatus.OK)
    public User getUserById(@PathVariable("id") String id) throws NotFoundException {
        return userService.getUser(id);
    }

    @GetMapping("/user/{id}/tweet")
    @ResponseStatus(HttpStatus.OK)
    public List<Tweet> getTweetsForUserId(@PathVariable("id") String id) throws NotFoundException {
        return tweetService.getTweetsForUser(id);
    }

    @PostMapping("/tweet")
    @ResponseStatus(HttpStatus.CREATED)
    public Tweet createTweet(@RequestBody TweetRequest tweetRequest) throws NotFoundException, BadRequestException {
        if (!isValid(tweetRequest)) {
            throw new BadRequestException("Must specify tweet and user");
        }
        return tweetService.createTweet(tweetRequest);
    }

    @GetMapping("/tweet")
    @ResponseStatus(HttpStatus.OK)
    public List<Tweet> getAllTweets() {
        return tweetService.getAllTweets();
    }

    @GetMapping("/tweet/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Tweet getTweetById(@PathVariable("id") String id) throws NotFoundException {
        return tweetService.getTweet(id);
    }

    @GetMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<Tweet> search(
        @RequestParam(value = "start_time", required = false) Long startTime,
        @RequestParam(value = "end_time", required = false) Long endTime,
        @RequestParam(value = "key_word", required = false) String keyWord
    ) throws BadRequestException {
        if (startTime == null && endTime == null && keyWord == null) {
            throw new BadRequestException("Must specify at least one of: start_time, end_time, key_word");
        }
        return tweetService.searchTweets(startTime, endTime, keyWord);
    }

    @ExceptionHandler(value = BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleBadRequest(BadRequestException e) {
        return new ErrorResponse(e);
    }

    @ExceptionHandler(value = DuplicateException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorResponse handleDuplicate(DuplicateException e) {
        return new ErrorResponse(e);
    }

    @ExceptionHandler(value = NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse handleNotFound(NotFoundException e) {
        return new ErrorResponse(e);
    }

    private boolean isValid(UserRequest userRequest) {
        return Strings.isNotEmpty(userRequest.getName());
    }

    private boolean isValid(TweetRequest tweetRequest) {
        return Strings.isNotEmpty(tweetRequest.getUser_name()) && Strings.isNotEmpty(tweetRequest.getTweet());
    }
}
