package com.elovee.twitterclient.repo;

import com.elovee.twitterclient.model.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class UserRepo implements Repository<User> {

    private HashMap<String, User> users;
    private HashMap<String, User> usersByName;

    public UserRepo() {
        initializeMaps();
    }

    @Override
    public List<User> getAll() {
        return new ArrayList<>(users.values());
    }

    @Override
    public User getById(String id) {
        return users.get(id);
    }

    public User getByName(String name) {
        return usersByName.get(name);
    }

    public boolean usernameExists(String username) {
        return usersByName.containsKey(username);
    }

    @Override
    public void deleteAll() {
        initializeMaps();
    }

    @Override
    public void save(User record) {
        users.put(record.getId(), record);
        usersByName.put(record.getName(), record);
    }

    private void initializeMaps() {
        this.users = new HashMap<>();
        this.usersByName = new HashMap<>();
    }
}
