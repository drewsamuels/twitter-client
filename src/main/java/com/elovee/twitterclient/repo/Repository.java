package com.elovee.twitterclient.repo;

import java.util.List;

public interface Repository<T> {

    List<T> getAll();

    T getById(String id);

    void deleteAll();

    void save(T record);

}
