package com.elovee.twitterclient.repo;

import com.elovee.twitterclient.model.Tweet;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class TweetRepo implements Repository<Tweet> {

    private HashMap<String, Tweet> tweets;

    public TweetRepo() {
        this.tweets = new HashMap<>();
    }

    @Override
    public List<Tweet> getAll() {
        return new ArrayList<>(tweets.values());
    }

    @Override
    public Tweet getById(String id) {
        return tweets.get(id);
    }

    @Override
    public void deleteAll() {
        this.tweets = new HashMap<>();
    }

    @Override
    public void save(Tweet record) {
        tweets.put(record.getId(), record);
    }
}
