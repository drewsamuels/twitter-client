package com.elovee.twitterclient.exception;

public class DuplicateException extends Exception {

    public DuplicateException(String message) {
        super(message);
    }
}
