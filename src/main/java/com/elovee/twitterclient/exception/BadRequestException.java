package com.elovee.twitterclient.exception;

public class BadRequestException extends Exception {

    public BadRequestException(String message) {
        super(message);
    }

}
