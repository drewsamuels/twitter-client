package com.elovee.twitterclient.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Tweet {
    private final String id = UUID.randomUUID().toString();
    private String userId;
    private String tweet;
    private long createdAt; //epoch milliseconds

    public Tweet(TweetRequest tweetRequest, String userId) {
        this.tweet = tweetRequest.getTweet();
        this.userId = userId;
        this.createdAt = Instant.now().toEpochMilli();
    }
}
