package com.elovee.twitterclient.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor //These constructors are required for the Jackson mapping library
@NoArgsConstructor
public class ErrorResponse {
    private String message;

    public ErrorResponse(Exception e) {
        this.message = e.getMessage();
    }
}
