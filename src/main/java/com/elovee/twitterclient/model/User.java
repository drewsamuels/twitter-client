package com.elovee.twitterclient.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedList;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {
    private final String id = UUID.randomUUID().toString();
    private String name;
    private LinkedList<String> tweetIds; //most recent first

    public User(UserRequest userRequest) {
        this.name = userRequest.getName();
        this.tweetIds = new LinkedList<>();
    }

    public void addTweet(Tweet tweet) {
        tweetIds.addFirst(tweet.getId());
    }
}
